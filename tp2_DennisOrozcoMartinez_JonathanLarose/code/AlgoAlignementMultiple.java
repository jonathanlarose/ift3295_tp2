

public class AlgoAlignementMultiple {

	public static void main(String [] args){

		//Partie II
		String acidesAmines = "ARNDCQEGHILKMFPSTWYVBZX-";

		String[] sequences = new String[5];
		sequences[0] = "MGEIGFTEKQEALVKESWEILKQDIPKYSLHFFSQILEIAPAAKGLFSFLRDSDEVPHNNPKLKAHAVKVFKMTCETAIQLREEGKVVVADTTLQYLGSIHLKSGVIDPHFEVVKEALLRTLKEGLGEKYNEEVEGAWSQAYDHLALAIKTEMKQEES";
		sequences[1] = "MEKVPGEMEIERRERSEELSEAERKAVQATWARLYANCEDVGVAILVRFFVNFPSAKQYFSQFKHMEEPLEMERSPQLRKHACRVMGALNTVVENLHDPEKVSSVLSLVGKAHALKHKVEPVYFKILSGVILEVIAEEFANDFPPETQRAWAKLRGLIYSHVTAAYKEVGWVQQVPNATTPPATLPSSGP";
		sequences[2] = "MVLSAADKNNVKGIFTKIAGHAEEYGAETLERMFTTYPPTKTYFPHFDLSHGSAQIKGHGKKVVAALIEAANHIDDIAGTLSKLSDLHAHKLRVDPVNFKLLGQCFLVVVAIHHPAALTPEVHASLDKFLCAVGTVLTAKYR";
		sequences[3] = "MGLSDGEWQLVLNVWGKVEADIPGHGQEVLIRLFKGHPETLEKFDKFKHLKSEDEMKASEDLKKHGATVLTALGGILKKKGHHEAEIKPLAQSHATKHKIPVKYLEFISECIIQVLQSKHPGDFGADAQGAMNKALELFRKDMASNYKELGFQG";
		sequences[4] = "MERLESELIRQSWRAVSRSPLEHGTVLFSRLFALEPSLLPLFQYNGRQFSSPEDCLSSPEFLDHIRKVMLVIDAAVTNVEDLSSLEEYLATLGRKHRAVGVRLSSFSTVGESLLYMLEKCLGPDFTPATRTAWSQLYGAVVQAMSRGWDGE";

		System.out.println("Alignement entre s2 et s1: ");
		AlignementGlobal matrice = new AlignementGlobal(sequences[1],sequences[0],10,1);
		System.out.println(matrice.getsAlign1());
		System.out.println(matrice.getsAlign2());
		System.out.println("score de cet alignement :" + matrice.getScore() +"\n");
		
		System.out.println("Alignement entre s2 et s3: ");
		AlignementGlobal matrice2 = new AlignementGlobal(sequences[1],sequences[2],10,1);
		System.out.println(matrice2.getsAlign1());
		System.out.println(matrice2.getsAlign2());
		System.out.println("score de cet alignement :" + matrice2.getScore() +"\n");
		
		System.out.println("Alignement entre s2 et s4: ");
		AlignementGlobal matrice3 = new AlignementGlobal(sequences[1],sequences[3],10,1);
		System.out.println(matrice3.getsAlign1());
		System.out.println(matrice3.getsAlign2());
		System.out.println("score de cet alignement :" + matrice3.getScore() +"\n");
		
		System.out.println("Alignement entre s2 et s5: ");
		AlignementGlobal matrice4 = new AlignementGlobal(sequences[1],sequences[4],10,1);
		System.out.println(matrice4.getsAlign1());
		System.out.println(matrice4.getsAlign2());
		System.out.println("score de cet alignement :" + matrice4.getScore() +"\n");
		
		String s1 = "MGEIGFTEKQEALVKESWEILKQDIPKYSLHFFSQILEIAPAAKGLFSFLRDSDEVPHNNPKLKAHAVKVFKMTCETAIQLREEGKVVVADTTLQYLGSIHLKSGV";
		String s2 = "MEKVPGEMEIERRERSEELSEAERKAVQATWARLYANCEDVGVAILVRFFVNFPSAKQYFSQFKHMEEPLEMERSPQLRKHACRVMGALNTVVENLHDPEKVS";
		AlignementGlobal matrice5 = new AlignementGlobal(s1,s2,10,1);
		System.out.println(matrice5.getsAlign1());
		System.out.println(matrice5.getsAlign2());
		System.out.println("score de cet alignement :" + matrice5.getScore() +"\n");
				
		
		AlignementMultiple multiple = new AlignementMultiple(sequences,10,1);
		System.out.print(multiple);

		System.out.println("seqC :\t" + multiple.getSeqConsensus());
	}
}


