/**
 * Created by Jonathan on 2017-10-29.
 */
public class AlgoRepliement2 {

    public static void main(String [] args) {

        String sequenceArn = "GCGUGCUUGCGUGCACG";

        if(args.length > 0)
            sequenceArn = args[0];

        Repliement r = new Repliement(sequenceArn);

        //Algo 2
        System.out.println("Algorithme pour le rempliement seulement avec scores d'empilements");
        r.calculRepliement2();

        System.out.println("\n" + r.matriceToString());

        System.out.println("Score : " + r.getScore());

        System.out.println(sequenceArn);
        System.out.println(r.getRepliementParenthese());
    }
}
