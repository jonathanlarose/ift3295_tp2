/**
 * Created by Jonathan on 2017-10-29.
 */
public class AlignementGlobalAffine {

    private String s1;
    private String s2;
    private String sAlign1;
    private String sAlign2;
    private int open;
    private int ext;
    private int score;
    private int[][] tableProgDynamique;

    //Blosum62 Hardcode temporaire
    private static String acidesAmines = "ARNDCQEGHILKMFPSTWYVBZX-";
    private static int[][] blosum62 =
            {           /*  A   R   N   D   C   Q   E   G   H   I   L   K   M   F   P   S   T   W   Y   V   B   Z   X   * */
                    /*A*/ { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4},
                    /*R*/ {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4},
                    /*N*/ {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4},
                    /*D*/ {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4},
                    /*C*/ { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4},
                    /*Q*/ {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4},
                    /*E*/ {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
                    /*G*/ { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4},
                    /*H*/ {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4},
                    /*I*/ {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4},
                    /*L*/ {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4},
                    /*K*/ {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4},
                    /*M*/ {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4},
                    /*F*/ {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4},
                    /*P*/ {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4},
                    /*S*/ { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4},
                    /*T*/ { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4},
                    /*W*/ {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4},
                    /*Y*/ {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4},
                    /*V*/ { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4},
                    /*B*/ {-2, -1,  3,  4, -3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4},
                    /*Z*/ {-1,  0,  0,  1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
                    /*X*/ { 0, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4},
                    /***/ {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1}
                    /*      A   R   N   D   C   Q   E   G   H   I   L   K   M   F   P   S   T   W   Y   V   B   Z   X   * */
            };

    //Getters
    public int getScore() {
        return score;
    }

    public String getS1() {
        return s1;
    }

    public String getS2() {
        return s2;
    }

    public String getsAlign1() {
        return sAlign1;
    }

    public String getsAlign2() {
        return sAlign2;
    }

    //Constructeur
    public AlignementGlobalAffine(String s1, String s2, int open, int ext){

        this.s1 = s1;
        this.s2 = s2;
        this.open = open;
        this.ext = ext;
        aligneSequences();
    }


    public void aligneSequences() {

        int[][] v = new int[s1.length()+1][s2.length()+1];
        int[][] e = new int[s1.length()+1][s2.length()+1];
        int[][] f = new int[s1.length()+1][s2.length()+1];

        //initialisation
        v[0][0] = 0;
        f[0][0] = 0;
        e[0][0] = 0;

        //colonne 0
        for(int i=1; i<= s1.length(); i++) {
            v[i][0] = -open - i*ext;
            f[i][0] = -open - i*ext;
            e[i][0] = -Integer.MAX_VALUE;
        }

        //ligne 0
        for(int i=1; i<= s2.length(); i++) {
            v[0][i] = -open - i*ext;
            f[0][i] = -Integer.MAX_VALUE;
            e[0][i] = -open - i*ext;
        }


        //Remplissage table de programmation dynamique
        for(int i=1; i<=s1.length(); i++){

            int max = -Integer.MAX_VALUE;

            for(int j=1; j<=s2.length(); j++){

                max = v[i-1][j-1] + scoreAlign(s1.charAt(i-1), s2.charAt(j-1));
                e[i][j] = ((e[i][j-1]) > (v[i][j-1]-open))? (e[i][j-1])-ext : (v[i][j-1]-open)-ext;
                f[i][j] = ((e[i-1][j]) > (v[i-1][j]-open))? (e[i-1][j])-ext : (v[i-1][j]-open)-ext;

                if(max < e[i][j])
                    max = e[i][j];

                if(max < f[i][j])
                    max = f[i][j];

                v[i][j] = max;
            }
        }

        this.tableProgDynamique = v;
        this.score = tableProgDynamique[s1.length()][s2.length()];

        /*//Backtracking et alignement
        String aS1 = "";
        String aS2 = "";

        int i = s1.length();
        int j = s2.length();

        while(i != 0 && j !=0){

            char pred;
            int max;

            //Trouve le predecesseur
            if(v[i][j] == e[i][j]){
                aS1 = "-" + aS1;
                aS2 = s2.charAt(j-1) + aS2;
                j--;
            }
            else if(v[i][j] == f[i][j]){
                aS1 = s1.charAt(i-1) + aS1;
                aS2 = "-" + aS2;
                i--;
            }
            else{
                aS1 = s1.charAt(i-1) + aS1;
                aS2 = s2.charAt(j-1) + aS2;
                j--;
                i--;
            }
        }

        this.sAlign1 = aS1;
        this.sAlign2 = aS2;*/
    }

    private void calculTableProgDynamique(){


    }

    private int scoreAlign(char c1, char c2){

        int index1 = acidesAmines.indexOf(c1);
        int index2 = acidesAmines.indexOf(c2);

        return blosum62[index1][index2];
    }

    private void backTracking(){


    }


    public String toString (){

        String resultat = "La table de programmation dynamique est : \n\n";
        String bordure = "";

        for(int i=0; i<=s2.length(); i++)
            bordure += "|----";

        bordure += "|";

        resultat += bordure + "\n";

        for (int i = 0; i <= s1.length(); i++){

            resultat += "|";

            for (int j = 0; j <= s2.length(); j++){

                String num = "" + tableProgDynamique[i][j];
                resultat += num;

                for(int k=4-num.length(); k>0; k--)
                    resultat += " ";

                resultat += "|";
            }

            resultat += "\n" + bordure + "\n";
        }
        return resultat;
    }
}
