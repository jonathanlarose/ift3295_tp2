

public class Repliement {
	
	private String sequenceArn;
	private String inverseComplement;
	private int[][] matriceDeScores;
	private String repliementParenthese;
	private boolean empilement;

	//Getters
	public String getInverseComplement(){
		return this.inverseComplement;
	}

	public int[][] getMatriceDeScores(){ return this.matriceDeScores; }

	public int getScore() {
		return matriceDeScores[0][0];
	}

	public String getRepliementParenthese() {
		return repliementParenthese;
	}

	public boolean isEmpilement() {
		return empilement;
	}

	// Constructeur
	public Repliement(String sequenceArn){
		
		this.sequenceArn = sequenceArn;
		calculInverseComplement();

		this.matriceDeScores = new int[sequenceArn.length()][sequenceArn.length()];

		this.empilement = true;
		calculRepliement1();
	}
	
	// Fonction qui trouve l'inverse et complement de la sequence de ARN 
	private void calculInverseComplement(){

		String ic = "";
		
		for(int i = 0; i < sequenceArn.length(); i++){
			
			switch(sequenceArn.charAt(i)){
				case 'A':
					ic = "U" + ic;
					break;
					
				case 'G':
					ic = "C" + ic;
					break;
					
				case 'C':
					ic = "G" + ic;
					break;
					
				case 'U':
					ic = "A" + ic;
					break;
			}
		}
		
		this.inverseComplement = ic;
	}


	public String calculRepliement1(){

		//Si déjà calculé
		if(!empilement)
			return this.repliementParenthese;

		this.empilement = false;

		calculMatriceDeScore1();

		return retrouverRepliement();
	}

	public String calculRepliement2(){

		//Si déjà calculé
		if(empilement)
			return this.repliementParenthese;

		this.empilement = true;

		calculMatriceDeScore2();

		return retrouverRepliement();
	}

	//Calcul et initialise la matrice de score et le score
	private void calculMatriceDeScore1(){

		int l = sequenceArn.length();

		//Rempli la table de programmation dynamique
		for(int i=l-1; i>=0; i--){
			for(int j=l-i-1; j>=0; j--){

				//Les anti-diagonales à 0
				if(i+j == l-1){
					matriceDeScores[i][j] = 0;

					if(i != 0)
						matriceDeScores[i][j+1] = 0;

					continue;
				}

				//On initialise la case courante avec la valeur max des voisins
				int p = (sequenceArn.charAt(i)==inverseComplement.charAt(j))? 1 : 0;
				int droite = matriceDeScores[i][j+1];
				int bas = matriceDeScores[i+1][j];
				int diag = matriceDeScores[i+1][j+1] + p;

				matriceDeScores[i][j] = (droite > bas)? droite : bas;
				matriceDeScores[i][j] = (diag > matriceDeScores[i][j])? diag : matriceDeScores[i][j];
			}
		}
	}

	//Calcul et initialise la matrice de score et le score
	private void calculMatriceDeScore2(){

		int l = sequenceArn.length();

		//Rempli la table de programmation dynamique
		for(int i=l-1; i>=0; i--){
			for(int j=l-i-1; j>=0; j--){

				//Les anti-diagonales à 0
				if(i+j == l-1){
					matriceDeScores[i][j] = 0;

					if(i != 0)
						matriceDeScores[i][j+1] = 0;

					continue;
				}

				//On initialise la case courante avec la valeur max des voisins
				int p = (sequenceArn.charAt(i)==inverseComplement.charAt(j))? 1 : -1;
				int mu = empilementScore(i,j);
				int droite = matriceDeScores[i][j+1]-1;
				int bas = matriceDeScores[i+1][j]-1;
				int diag = matriceDeScores[i+1][j+1] + p + mu;

				matriceDeScores[i][j] = (droite > bas)? droite : bas;
				matriceDeScores[i][j] = (diag > matriceDeScores[i][j])? diag : matriceDeScores[i][j];
			}
		}
	}

	private int empilementScore(int i, int j){

		int s = 0;

		//Cas ou on à coté de l'anti-diagonale, on ne compte pas l'empilement
		if(i+j+2 >= sequenceArn.length())
			return 0;

		char i1 = sequenceArn.charAt(i);
		char j1 = inverseComplement.charAt(j);
		char i2 = sequenceArn.charAt(i+1);
		char j2 = inverseComplement.charAt(j+1);

		if(i1==j1 && i2==j2){
			if(i1!=i2)
				s = 1;
			else if(i1=='G' || i1=='C')
				s = 2;
		}

		return s;
	}

	//Fonction qui retourne l'expression parentheser du repliement optimale à partir de la table de programmation dynamique
	private String retrouverRepliement(){

		String parenthese = "";
		String deb = "";
		String fin = "";
		int [][] m = this.matriceDeScores;

		int i = 0;
		int j = 0;

		while(i+j <= sequenceArn.length()-1){

			int v = m[i][j];

			if(m[i+1][j]==v) {
				deb += ".";
				i++;
				continue;
			}
			else if(m[i][j+1]==v){
				fin = "."+fin;
				j++;
				continue;
			}
			else{
				deb += "(";
				fin = ")"+fin;
				i++;
				j++;
			}
		}

		parenthese = deb + fin;
		this.repliementParenthese = parenthese;

		return parenthese;
	}

	public String matriceToString (){

		String resultat = "La table de programmation dynamique est : \n\n";
		String bordure = "";

		for(int i=0; i<sequenceArn.length(); i++)
			bordure += "|----";

		bordure += "|";

		resultat += bordure + "\n";

		for (int i = 0; i < sequenceArn.length(); i++){

			resultat += "|";

			for (int j = 0; j < sequenceArn.length(); j++){

				resultat += matriceDeScores[i][j];

				if (matriceDeScores[i][j] >= 0 && matriceDeScores[i][j] <= 9)
					resultat += "   ";

				else
					resultat += "  ";


				resultat += "|";
			}

			resultat += "\n" + bordure + "\n";
		}
		return resultat;
	}
}
