/**
 * Created by dennisorozco on 17-10-24.
**/
public class AlignementMultiple {

    private static String acidesAmines = "ARNDCQEGHILKMFPSTWYVBZX-";
    private int open;
    private int ext;                            // extension de gap
    private String[] sequences;                 // sequences à comparer
    private AlignementGlobal [][] alignGlobaux; // matrice de scores des alignements paire à paire
    private String[] alignMultiple;             //Colonne 1 : numero de la sequence, colonne 2 alignement
    private int l;                              // nombre de sequences à aligner
    private int scoreSp;
    private String seqConsensus;

    //Getters
    public AlignementGlobal[][] getAlignGlobaux() {
        return alignGlobaux;
    }

    public String getSeqConsensus() {
        return seqConsensus;
    }

    public int getScoreSp() {
        return scoreSp;
    }

    public AlignementMultiple(String[] sequences, int ouverture, int extension){
        this.open = ouverture;
        this.ext = extension;
        this.l = sequences.length;
        this.sequences = sequences;
        this.alignGlobaux = new AlignementGlobal[l][l];

        calculAligneMultiple();
        //calculScoreSp();
        //calculSequenceConsensus();
    }

    //Alignement multiple
    private void calculAligneMultiple(){

        //Alignement pair à pair pour chaque sequences
        alignementGlobaux();

        //On trouve la sequence centrale
        int sc = trouverSequenceCentrale();
        System.out.println("La sequence centrale est : " + (sc+1));


        //Matrice d'alignement multiple
        String[] mAlign = new String[sequences.length];

        for(int i=0; i<l; i++){

            AlignementGlobal a = alignGlobaux[sc][i];

            if(a == null)
                continue;


            if(mAlign[sc]==null) {
                mAlign[sc] = a.getsAlign1();
                mAlign[i] = a.getsAlign2();
                continue;
            }

            if(mAlign[sc].equals(a.getsAlign1())){
                mAlign[i] = a.getsAlign2();
                continue;
            }
            else if(mAlign[sc].length() > a.getsAlign1().length()){

                AlignementGlobal s = new AlignementGlobal(mAlign[sc], a.getS2(), open, ext);
                mAlign[i] = s.getsAlign2();
            }
            else{

                mAlign[i] = a.getsAlign2();

                AlignementGlobal s = new AlignementGlobal(mAlign[sc], a.getsAlign1(), open, ext);
            }
        }

        this.alignMultiple = mAlign;
    }

    private String[] addGap(String[] m, int central, int pos){

        for(int i=0; i<l; i++){

            if(m[i] == null || i==central)
                continue;

            m[i] = m[i].substring(0,pos) + "-" + m[i].substring(pos);
        }

        return m;
    }

    //Fonction qui crée une matrice des alignements global pair à pair pour chaque pair de sequences
    private void alignementGlobaux(){

        for(int i=0; i<l; i++){
            for(int j=0; j<l; j++){

                if(i==j)
                    continue;

                alignGlobaux[i][j] = new AlignementGlobal(sequences[i], sequences[j], open, ext);
            }
        }
    }

    //Fonction qui retrouve la séquence central à partir de la matrice de score
    private int trouverSequenceCentrale(){

        int sc = 0;
        int max = 0;
        int sum;

        for(int i=0; i<l; i++){

            sum = 0;

            for(int j=0; j<l; j++) {

                if(i==j)
                    continue;

                sum += alignGlobaux[i][j].getScore();
            }

            if(max < sum) {
                max = sum;
                sc = i;
            }
        }

        return sc;
    }

    private void calculScoreSp(){

        int sp = 0;

        for(String s : alignMultiple){
            for(String sCompare : alignMultiple){

                int sum = 0;

                if(s.equals(sCompare))
                    continue;

                for(int i=0; i<s.length(); i++)
                    sum += (s.charAt(i)==sCompare.charAt(i))? 1 : 0;

                sp += sum;
            }
        }

        this.scoreSp = sp;
    }

    private void calculSequenceConsensus(){

        int sl = alignMultiple[0].length();
        int[][] mConcensus = new int[sl][acidesAmines.length()];
        String sc = "";

        //Rempli la matrice concensus
        for(int i=0; i<sl; i++){

            for(String s : alignMultiple){

                char c = s.charAt(i);

                if(c == '-')
                    continue;

                int index = acidesAmines.indexOf(c);

                mConcensus[i][index]++;
            }
        }

        //Trouve la sequence concensus
        for(int i=0; i<sl; i++){

            char c = '-';
            int max = -1;

            for(int j=0; j<acidesAmines.length(); j++){

                if(max < mConcensus[i][j]){
                    c = acidesAmines.charAt(j);
                    max = mConcensus[i][j];
                }
            }

            sc += c;
        }

        this.seqConsensus = sc;
    }

    public String toString(){

        String s = "";

        for(int i=0; i<l; i++)
            s += "seq" + (i + 1) + " :\t" + alignMultiple[i] + "\n";

        return s;
    }
}



