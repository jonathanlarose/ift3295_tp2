/**
 * Created by Jonathan on 2017-10-28.
 */
public class AlignementGlobal {

    private String s1;
    private String s2;
    private String sAlign1;
    private String sAlign2;
    private int open;
    private int ext;
    private int score;

    //Blosum62 Hardcode temporaire
    private static String acidesAmines = "ARNDCQEGHILKMFPSTWYVBZX-";
    private static int[][] blosum62 =
            {           /*  A   R   N   D   C   Q   E   G   H   I   L   K   M   F   P   S   T   W   Y   V   B   Z   X   * */
                    /*A*/ { 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4},
                    /*R*/ {-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4},
                    /*N*/ {-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4},
                    /*D*/ {-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4},
                    /*C*/ { 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4},
                    /*Q*/ {-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4},
                    /*E*/ {-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
                    /*G*/ { 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4},
                    /*H*/ {-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4},
                    /*I*/ {-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4},
                    /*L*/ {-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4},
                    /*K*/ {-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4},
                    /*M*/ {-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4},
                    /*F*/ {-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4},
                    /*P*/ {-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4},
                    /*S*/ { 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4},
                    /*T*/ { 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4},
                    /*W*/ {-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4},
                    /*Y*/ {-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4},
                    /*V*/ { 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4},
                    /*B*/ {-2, -1,  3,  4, -3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4},
                    /*Z*/ {-1,  0,  0,  1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4},
                    /*X*/ { 0, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4},
                    /***/ {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1}
                    /*      A   R   N   D   C   Q   E   G   H   I   L   K   M   F   P   S   T   W   Y   V   B   Z   X   * */
            };

    //Getters
    public int getScore() {
        return score;
    }

    public String getS1() {
        return s1;
    }

    public String getS2() {
        return s2;
    }

    public String getsAlign1() {
        return sAlign1;
    }

    public String getsAlign2() {
        return sAlign2;
    }

    //Constructeur
    public AlignementGlobal(String s1, String s2, int open, int ext){

        this.s1 = s1;
        this.s2 = s2;
        this.open = open;
        this.ext = ext;
        aligneSequences();
    }

    /**
     * Fonction qui calcule un alignement affin de gap entre deux séquences
     */
    public void aligneSequences(){

        int[][][] M = new int[3][s1.length()+1][s2.length()+1];         // matrice pour les maximums
        int[][][] aligne = new int[3][s1.length()+1][s2.length()+1];    // matrice pour le backtracking

        // Initialisation des matrices

        for(int i = 1; i <= s1.length(); ++i) {
            M[0][i][0] = - (open + (i-1) * ext);
            M[1][i][0] = - (open + (i-1) * ext);
            M[2][i][0] = -10 * open;
            aligne[0][i][0] = 0;
            aligne[1][i][0] = 0;
            aligne[2][i][0] = 0;
        }

        for(int j = 1; j <= s2.length(); ++j) {

            M[0][0][j] = - 10 * open;
            M[1][0][j] = - (open + (j-1) * ext);
            M[2][0][j] = - (open + (j-1) * ext);
            aligne[0][0][j] = 1;
            aligne[1][0][j] = 1;
            aligne[2][0][j] = 1;
        }

        // Remlissage des matrices
        for(int i = 1; i <= s1.length(); ++i) {

            for(int j = 1; j <= s2.length(); ++j) {

                int low1 = M[0][i-1][j] - ext;
                int low2 = M[1][i-1][j] - open;

                if(low1 > low2) {

                    M[0][i][j] = low1;
                    aligne[0][i][j] = 0;
                }

                else {

                    M[0][i][j] = low2;
                    aligne[0][i][j] = 1;
                }

                int up1 = M[2][i][j-1] - ext;
                int up2 = M[1][i][j-1] - open;

                if(up1 > up2) {

                    M[2][i][j] = up1;
                    aligne[2][i][j] = 0;
                }

                else {

                    M[2][i][j] = up2;
                    aligne[2][i][j] = 1;
                }

                int opt1 = M[0][i][j];                                                  // Alignement de type 1 : gap s1 char s2
                int opt2 = M[1][i-1][j-1] + scoreAlign(s1.charAt(i-1),s2.charAt(j-1));  // ALignement de type 2 : char s1 char s2
                int opt3 = M[2][i][j];                                                  // Alignement de type 3 : char s1 gap s2

                M[1][i][j] = opt1;
                aligne[1][i][j] = 0;

                if(opt2 > M[1][i][j]) {

                    M[1][i][j] = opt2;
                    aligne[1][i][j] = 1;
                }

                if(opt3 > M[1][i][j]) {

                    M[1][i][j] = opt3;
                    aligne[1][i][j] = 2;
                }
            }
        }

        // Récuperation du score
        // Définition du type d'alignement
        int i = s1.length();
        int j = s2.length();
        int bestIJ = 0;
        int best = M[0][i][j];

        if(M[1][i][j] > best) {

            best = M[1][i][j];
            bestIJ = 1;
        }

        if(M[2][i][j] > best) {

            best = M[2][i][j];
            bestIJ = 2;
        }

        score =  best;
        sAlign1 = "";
        sAlign2 = "";
        // Définition du sens de parcours de la matrice
        // construction de l'alignement correspondant
        while(i > 0 && j > 0) {

            if(bestIJ == 0) {

                if(aligne[0][i][j] == 1) {
                    bestIJ = 1;
                }

                sAlign1 = s1.charAt(i-- - 1) + sAlign1;
                sAlign2 = '-' + sAlign2;
            }

            else if(bestIJ == 1) {

                if(aligne[1][i][j] == 0) {
                    bestIJ = 0;
                }

                else if(aligne[1][i][j] == 2) {
                    bestIJ = 2;
                }

                else {

                    sAlign1 = s1.charAt(i-- - 1) + sAlign1;
                    sAlign2 = s2.charAt(j-- - 1) + sAlign2;
                }

            } else {

                if(aligne[2][i][j] == 1) {
                    bestIJ = 1;
                }
                sAlign1 = '-' + sAlign1;
                sAlign2 = s2.charAt(j-- - 1) + sAlign2;
            }
        }

        if(i > 0) {

            sAlign1 = s1.substring(0,i) + sAlign1;
            String add = "";

            for(int x = 0; x < i; x++) {
                add += '-';
            }
            sAlign2 = add + sAlign2;
        }

        if(j > 0) {

            sAlign2 = s2.substring(0,j) + sAlign2;
            String add = "";

            for(int x = 0; x < j; x++) {
                add += '-';
            }
            sAlign1 = add + sAlign1;
        }

        /*int score2 = 0;

        // calcul du score optimal à partir des séquences alignées
        for (int w = 0; w < sAlign1.length(); w++) {

            if (sAlign1.charAt(w) != '-' && sAlign2.charAt(w) != '-') {
                score2 += scoreAlign(sAlign1.charAt(w), sAlign2.charAt(w));

            } else {

                score2 -= (open + ext);

                while (sAlign1.charAt(w+1) == '-' || sAlign2.charAt(w+1) == '-'){

                    score2 -= ext;
                    w++;

                    if (w+1 >= sAlign1.length()){
                        break;
                    }
                }
            }
        }

        score = score2;*/
    }

    // Méthode pour obtenir le score entre des acides aminés ains que des gaps
    private int scoreAlign(char c1, char c2){

        int index1 = acidesAmines.indexOf(c1);
        int index2 = acidesAmines.indexOf(c2);

        return blosum62[index1][index2];
    }
}
