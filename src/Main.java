

public class Main {
	public static void main(String [] args){

		/*//Partie I
		String sequenceArn = "GCGUGCUUGCGUGCACG";
		Repliement r = new Repliement(sequenceArn);

		System.out.println("PARTIE I :\n");

		//Algo1
		System.out.println("Algorithme pour le rempliement seulement avec les appariements\n");
		System.out.println(r.matriceToString());

		System.out.println("Score : " + r.getScore());

		System.out.println(sequenceArn);
		System.out.println(r.getRepliementParenthese());
		//System.out.println(r.getInverseComplement());

		//Algo 2
		System.out.println("\n\nAlgorithme pour le rempliement seulement avec scores d'empilements");
		r.calculRepliement2();

		System.out.println("\n" + r.matriceToString());

		System.out.println("Score : " + r.getScore());

		System.out.println(sequenceArn);
		System.out.println(r.getRepliementParenthese());
		//System.out.println(r.getInverseComplement());*/

		//Partie II

		//Parsing du fichier de sequences.fasta

		String pathFichier = "sequences.fasta";
		String [] sequences2 = Parse.parse(pathFichier);

		//Partie II
		String acidesAmines = "ARNDCQEGHILKMFPSTWYVBZX-";

		String[] sequences = new String[5];
		sequences[0] = "MGEIGFTEKQEALVKESWEILKQDIPKYSLHFFSQILEIAPAAKGLFSFLRDSDEVPHNNPKLKAHAVKVFKMTCETAIQLREEGKVVVADTTLQYLGSIHLKSGVIDPHFEVVKEALLRTLKEGLGEKYNEEVEGAWSQAYDHLALAIKTEMKQEES";
		sequences[1] = "MEKVPGEMEIERRERSEELSEAERKAVQATWARLYANCEDVGVAILVRFFVNFPSAKQYFSQFKHMEEPLEMERSPQLRKHACRVMGALNTVVENLHDPEKVSSVLSLVGKAHALKHKVEPVYFKILSGVILEVIAEEFANDFPPETQRAWAKLRGLIYSHVTAAYKEVGWVQQVPNATTPPATLPSSGP";
		sequences[2] = "MVLSAADKNNVKGIFTKIAGHAEEYGAETLERMFTTYPPTKTYFPHFDLSHGSAQIKGHGKKVVAALIEAANHIDDIAGTLSKLSDLHAHKLRVDPVNFKLLGQCFLVVVAIHHPAALTPEVHASLDKFLCAVGTVLTAKYR";
		sequences[3] = "MGLSDGEWQLVLNVWGKVEADIPGHGQEVLIRLFKGHPETLEKFDKFKHLKSEDEMKASEDLKKHGATVLTALGGILKKKGHHEAEIKPLAQSHATKHKIPVKYLEFISECIIQVLQSKHPGDFGADAQGAMNKALELFRKDMASNYKELGFQG";
		sequences[4] = "MERLESELIRQSWRAVSRSPLEHGTVLFSRLFALEPSLLPLFQYNGRQFSSPEDCLSSPEFLDHIRKVMLVIDAAVTNVEDLSSLEEYLATLGRKHRAVGVRLSSFSTVGESLLYMLEKCLGPDFTPATRTAWSQLYGAVVQAMSRGWDGE";


/*
AlignementMultiple multiple = new AlignementMultiple(sequences,10,1,acidesAmines);

		String[]s2s1 = multiple.aligneSequences(sequences[1],sequences[0]);
		String[]s2s3 = multiple.aligneSequences(sequences[1],sequences[2]);
		String[]s2s4 = multiple.aligneSequences(sequences[1],sequences[3]);
		String[]s2s5 = multiple.aligneSequences(sequences[1],sequences[4]);

		String matrScores = multiple.matriceScores();
		System.out.println(matrScores);

		System.out.println("alignement entre s2 et s1 : \n");
		System.out.println(s2s1[1]);
		System.out.println(s2s1[2]);
		System.out.println();

		System.out.println("alignement entre s2 et s3 : \n");
		System.out.println(s2s3[1]);
		System.out.println(s2s3[2]);
		System.out.println();

		System.out.println("alignement entre s2 et s4 : \n");
		System.out.println(s2s4[1]);
		System.out.println(s2s4[2]);
		System.out.println();

		System.out.println("alignement entre s2 et s5 : \n");
		System.out.println(s2s5[1]);
		System.out.println(s2s5[2]);
		System.out.println();

		System.out.println("l'alignement multiple correspondant est :\n");
		String [] sqNonAlignees = new String[sequences.length];
		sqNonAlignees[0] = s2s1[1];
		sqNonAlignees[1] = s2s1[2];
		sqNonAlignees[2] = s2s3[2];
		sqNonAlignees[3] = s2s4[2];
		sqNonAlignees[4] = s2s5[2];

		String sqAlignes = AlignementMultiple.alignetMultiple(sqNonAlignees);
		System.out.println(sqAlignes);
		System.out.println();


		String[] sequences = new String[5];
		sequences[0] = "MGEIGFTEKQEALVKESWEILKQDIPKYSLHFFSQILEIAPAAKGLFSFLRDSDEVPHNNPKLKAHAVKVFKMTCETAIQLREEGKVVVADTTLQYLGSIHLKSGVIDPHFEVVKEALLRTLKEGLGEKYNEEVEGAWSQAYDHLALAIKTEMKQEES";
		sequences[1] = "MEKVPGEMEIERRERSEELSEAERKAVQATWARLYANCEDVGVAILVRFFVNFPSAKQYFSQFKHMEEPLEMERSPQLRKHACRVMGALNTVVENLHDPEKVSSVLSLVGKAHALKHKVEPVYFKILSGVILEVIAEEFANDFPPETQRAWAKLRGLIYSHVTAAYKEVGWVQQVPNATTPPATLPSSGP";
		sequences[2] = "MVLSAADKNNVKGIFTKIAGHAEEYGAETLERMFTTYPPTKTYFPHFDLSHGSAQIKGHGKKVVAALIEAANHIDDIAGTLSKLSDLHAHKLRVDPVNFKLLGQCFLVVVAIHHPAALTPEVHASLDKFLCAVGTVLTAKYR";
		sequences[3] = "MGLSDGEWQLVLNVWGKVEADIPGHGQEVLIRLFKGHPETLEKFDKFKHLKSEDEMKASEDLKKHGATVLTALGGILKKKGHHEAEIKPLAQSHATKHKIPVKYLEFISECIIQVLQSKHPGDFGADAQGAMNKALELFRKDMASNYKELGFQG";
		sequences[4] = "MERLESELIRQSWRAVSRSPLEHGTVLFSRLFALEPSLLPLFQYNGRQFSSPEDCLSSPEFLDHIRKVMLVIDAAVTNVEDLSSLEEYLATLGRKHRAVGVRLSSFSTVGESLLYMLEKCLGPDFTPATRTAWSQLYGAVVQAMSRGWDGE";

		AlignementMultiple multiple = new AlignementMultiple(sequences,10,1);
		System.out.print(multiple);

 */

	}
}


